<?php
/*
功能：上传一个bin文件，从bin文件的前64个字节提取bin文件的版本信息
bin前64的格式:
struct{
	int h_magic,
	int h_hcrc,
	int h_time,
	int h_size,
	int h_load,
	int h_ep,
	int h_dcrc,
	unsigned char h_os,
	unsigned char h_arch,
	unsigned char h_type,
	unsigned char h_comp,
	char h_name[32]
}
存入的list.php配置文件，提供上传、删除功能。

提供API功能，传入参数index.php?act=api&mac=&ver=UVR1001，可以获取最新UVR1001的版本
提供MAC地址过滤功能，每条记录的mac可以增删改查

常量URL的作用，用于API返回的下载版本文件的地址
*/
date_default_timezone_set('Asia/Shanghai');
header('Content-type: text/html; charset=utf-8');
define('URL', 'http://192.168.1.1/');

$list = require_once('bin/list.php');
$maclist = array();
$id = 0;
$macid = 0;

function upload_file($form_name='file1',$savepath='./bin/',$allowExt='bin|binary',$allowSize=20480000)
{
    if(!isset($_FILES[$form_name]['name']) || $_FILES[$form_name]['size']<=0)
    {
        return array(1,'没有找到需要上传的文件');
    }
    $type = strtolower($_FILES[$form_name]['type']);
    $type = explode('/', $type);
    $type = $type[1];
    
    // if(false === strpos($allowExt, $type))
    // {
    //     return array(2,'不允许上传文件类型'.$type);
    // }
    if($_FILES[$form_name]['size'] > $allowSize)
    {
        $k = $allowSize / 1024;
        if($k>1024)
        {
            $k = $k/1024;
            $k = $k.'M';
        }
        else
        {
            $k = $k .'K';
        }
        return array(3,'上传的文件太大，不能超过'.$k);
    }
    $filename = time().'_'.rand(1000,9999).'.bin';
    if(!file_exists($savepath))
    {
        @mkdir($savepath);
    }
    $file = $savepath.$filename;
    $result = @move_uploaded_file($_FILES[$form_name]['tmp_name'], $file);

    if(!$result)
    {
        return array(4,'移动文件失败，请确认目录存在并且有读写权限');
    }

    return array(0,$file);
}

function updateList()
{
	global $list;
	@copy('bin/list.php','bin/list.php.bak');
	$content = var_export($list,true);
	$content = '<?php return '.$content.';?>';
	@$re = file_put_contents('bin/list.php', $content);
	if($re)
	{
		return true;
	}
	return false;
}

function redirect($url = 'index.php')
{
	Header('Location: '.$url);
	exit(0);
}

function p($msg,$back=true)
{
	if($back)
	{
		die($msg.' <a href="javascript:history.go(-1);">返回</a>');
	}
	else
	{
		die($msg);
	}
}

function out($result)
{
	$callback = isset($_GET['callback'])?trim($_GET['callback']):'';
	$content = json_encode($result);
	if($callback)
	{
		die($callback.'('.$content.')');
	}
	else
	{
		die($content);
	}
}

function info($path)
{
	/* item的值如下
	array(12) {
	  ["h_magic"]=>
	  int(654645590)
	  ["h_hcrc"]=>
	  int(963628990)
	  ["h_time"]=>
	  int(1415606689)
	  ["h_size"]=>
	  int(1076019)
	  ["h_load"]=>
	  int(2147483648)
	  ["h_ep"]=>
	  int(2147483648)
	  ["h_dcrc"]=>
	  int(1450926855)
	  ["h_os"]=>
	  int(5)
	  ["h_arch"]=>
	  int(5)
	  ["h_type"]=>
	  int(2)
	  ["h_comp"]=>
	  int(3)
	  ["h_name"]=>
	  string(13) "UVR1001-0.0.1"
	}
	*/
	$format = 'Nh_magic/Nh_hcrc/Nh_time/Nh_size/Nh_load/Nh_ep/Nh_dcrc/Ch_os/Ch_arch/Ch_type/Ch_comp/a32h_name'; 
	$length = 64;

	$data = file_get_contents($path, 'r'); 
	$i = 0;
	$bianbian = unpack("@$i/$format", $data); 	
	foreach ($bianbian as &$value) 
	{  
	    if (is_string($value)) 
	    {  
	        $value = strtok($value, "\0"); 
	    }  
	}  
	return $bianbian;
}

$act = isset($_REQUEST['act'])?trim($_REQUEST['act']):'';
$sact = isset($_GET['sact'])?trim($_GET['sact']):'';

//上传
if($act == 'save')
{
	$type = $_POST['type'];
	$re = upload_file();
	if($re[0] != 0)
	{
		p($re[1]);
	}
	$file = $re[1];
	$info = info($file);
	$ver_arr = split('-',$info['h_name']);
	$info['type'] = $type;
	$info['file'] = $file;
	$info['ver_name'] = $ver_arr[0];
	$info['ver_no'] = $ver_arr[1];
	$list[] = $info;
	$re = updateList();
	if(!$re)
	{
		p('更新列表失败，请确认当前目录是否有写权限');
	}
	redirect();
}
else if($act=='del')
{
	$id = $_GET['id'];
	$file = $list[$id-1]['file'];
	@unlink($file);
	unset($list[$id]);
	if(!updateList())
	{
		p('更新列表失败，请确认当前目录是否有写权限');
	}
	redirect();
}
else if($act == 'type')
{
	$id = $_GET['id'];
	$type = $_GET['type'];
	$list[$id-1]['type'] = $type;
	$re = updateList();
	if(!$re)
	{
		p('更新列表失败，请确认当前目录是否有写权限');
	}
	redirect();
}
else if($act == 'api')
{
	$result = array('status'=>0,'msg'=>'','data'=>array());	
	$mac = isset($_GET['mac'])?trim($_GET['mac']):'';
	$ver = isset($_GET['v'])?trim($_GET['v']):'';
	//参数错误
	if(!$ver)
	{
		p('',false);
	}
	//没有更新
	if(count($list)<1)
	{
		p('',false);
	}
	$find = false;
	foreach ($list as $v) 
	{
		$maclist = $v['mac'];
		//mac 过滤
		if(is_array($maclist) && in_array($mac, $maclist))
		{
			continue;
		}
		if($ver == $v['ver_name'])
		{
			if(!$find || $find['ver_no'] < $v['ver_no'])
			{
				$find = $v;
			}
		}
	}
	$msg = '';
	if($find)
	{
		$msg = $find['h_name'].'|'.URL.$find['file'];
	}
	p($msg,false);	
}
else if($act == 'mac')
{
	$id = $_GET['id'];
	$macid = isset($_GET['macid'])?intval($_GET['macid']):0;	
	if(isset($list[$id-1]['mac']))
	{
		$maclist = $list[$id-1]['mac'];	
	}

	if($sact == 'del')
	{
		unset($maclist[$macid-1]);
		$list[$id-1]['mac'] = $maclist;
		if(!updateList())
		{
			p('更新列表失败，请确认当前目录是否有写权限');
		}
		redirect('index.php?act=mac&id='.$id);
	}
	else if($sact == 'save')
	{
		$mac = $_POST['mac'];
		if(!$mac)
		{
			p('MAC地址不能为空');
		}
		if($macid>0)
		{
			$maclist[$macid-1] = $mac;
		}
		else
		{
			$maclist[] = $mac;
		}
		$list[$id-1]['mac'] = $maclist;
		if(!updateList())
		{
			p('更新列表失败，请确认当前目录是否有写权限');
		}
		redirect('index.php?act=mac&id='.$id);

	}
}

?>
<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="zh-cn"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="zh-cn"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="zh-cn"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html class="not-ie" lang="zh-cn">
<!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">    
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- 新 Bootstrap 核心 CSS 文件 -->  

    <title>版本管理程序</title>
    <meta name="description" content="">
    <meta name="author" content="">
    <style type="text/css">
    	body{margin:0; padding:0; border:0px none; background:#fff; color:#000; font:75%/150% arial,'Lucida Grande','Lucida Sans Unicode',verdana,sans-serif,'宋体'; line-height:1.5em }
	 table,td,div,p {word-wrap:break-word; word-break:break-all; word-break/* */:normal }
	 td,th{overflow:hidden; padding:3px 5px }
		
	 a {color:#0b4a9f; text-decoration:none }
	 a:hover {color:#f50; text-decoration:none }

	 h1{margin:0; font-size:1.5em }
	 h2{margin:0; font-size:1.2em }
	 h3{margin:0; font-size:1em }
	 ul{margin:0; padding:0; list-style-type:none }
	 li{margin:0; list-style-type:none }
	 form {margin:0 }
	 p{margin:0 }
	 select,input{font-family:Arial, Helvetica, sans-serif }
	 img {border:0 } 
	 
	 .img {border:1px solid #cccccc} 
	 .yellow {color:#e35b00 }
	 .red {color:#b42503 }
	 .gray {color:#666666 }
	 .clear {overflow:hidden; height:10px }
	 .bold{font-weight: 800}
    </style>
</head>
<body>
<h1 style="text-align:center; display:block; height:100px; line-height:100px; font-size:30px">版本管理程序</h1>
<?php
if($act == 'edit')
{
?>
<form method="post" action="index.php?act=save" ENCTYPE="multipart/form-data">
<table width="80%" height="90%" bordercolor="#999999"  border="1" style="border:1px solid #999999" cellspacing="0" cellpadding="0" align="center">
	<tr style="background-color:#cccccc">
		<th colspan="5" style="border:0px" align="left">添加版本</th>
	</tr>
	<tr>
		<td width="15%" align="right" class="bold">类型:</td>
		<td width="15%">
			<select name="type">
				<option value="Beta">Beta</option>
				<option value="Release">Release</option>
			</select>
		</td>
		<td width="15%" align="right" class="bold">上传文件：</td>
		<td width="45%"><input type="file" name="file1" />(最大上传文件限制:<span class="red"><?php echo ini_get('upload_max_filesize'); ?></span>)</td>
		<td width="10%"><input type="submit" value="提交" />&nbsp;<a href="index.php">取消</a></td>
	</tr>

</table>
</form>
<br />
<?php
}
?>
<table width="80%" height="30" border="0" cellspacing="0" cellpadding="0" align="center">
	<tr><td><a href="index.php?act=edit">增加版本</a></td></tr>
</table>
<table width="80%" height="auto" bordercolor="#999999" border="1" cellspacing="0" cellpadding="0" align="center">

	<tr style="background-color:#cccccc;">
		<th width="10%">序号</th>
		<th>版本</th>
		<th width="30%">发布时间</th>
		<th width="15%">版本类型</th>
		<th width="30%">操作</th>
	</tr>
	<?php
	for($i=0;$i<count($list);$i++)
	{
		$v = $list[$i];
		$id = $i + 1;
		$d = date('Y-m-d H:i:s',$v['h_time']);
		$type = $v['type'];
		$totype = 'Beta';
		if($type == 'Beta')
		{
			$totype = 'Release';
		}
		echo "<tr>\n";
		echo "<td>{$id}</td>\n";
		echo "<td>{$v['h_name']}</td>\n";
		echo "<td>{$d}</td>\n";
		echo "<td><a href=\"index.php?act=type&id={$id}&type={$totype}\" onclick=\"return confirm('确认为更新版本的状态吗？');\">{$type}</a></td>\n";
		echo "<td><a href=\"index.php?act=mac&id={$id}\">过滤MAC</a>&nbsp;|&nbsp;<a href=\"index.php?act=del&id={$id}\" onclick=\"return confirm('确认要删除吗？');\">删除</a></td>\n";
		echo "</tr>\n";
	}
	?>
</table>

<?php
if($act == 'mac' && $sact == 'edit')
{
	$mac = '';
	$actname = '增加';
	if($macid > 0)
	{
		$mac = $maclist[$macid-1];
		$actname = '编辑';
	}
		
?>
<br />
<form method="post"  action="index.php?act=mac&sact=save&id=<?php echo $id;?>&macid=<?php echo $macid;?>">
<table width="80%" height="auto" bordercolor="#999999"  border="1" cellspacing="0" cellpadding="0" align="center">
	<tr style="background-color:#cccccc">
		<th colspan="3" align="left"><?php echo $actname; ?>MAC地址</th>
	</tr>
	<tr>
		<td width="20%" align="right" class="bold">MAC地址:</td>
		<td width="50%">
			<input type="text" name="mac" value="<?php echo $mac;?>" style="display:block;height:100%;border:0px;width:100%"/>
		</td>	
		<td width="30%"><input type="submit" value="提交" />&nbsp;<a href="index.php?act=mac&id=<?php echo $id;?>">取消</a></td>
	</tr>

</table>
</form>
<?php
}
if($act == 'mac')
{
?>
<br />
<table width="80%" height="30" border="0" cellspacing="0" cellpadding="0" align="center">
	<tr><td><a href="index.php?act=mac&sact=edit&id=<?php echo $id;?>">增加MAC</a></td></tr>
</table>
<table width="80%" height="auto" bordercolor="#999999"  border="1" cellspacing="0" cellpadding="0" align="center">
	
	<tr style="background-color:#cccccc">
		<th width="10%">序号</th>
		<th>MAC地址</th>	
		<th width="30%">操作</th>
	</tr>
	<?php
	for($i=0;$i<count($maclist);$i++)
	{
		$v = $maclist[$i];
		$macid = $i + 1;		
		echo "<tr>\n";
		echo "<td>{$macid}</td>\n";
		echo "<td>{$v}</td>\n";		
		echo "<td><a href=\"index.php?act=mac&sact=edit&id={$id}&macid={$macid}\">编辑</a>&nbsp;|&nbsp;<a href=\"index.php?act=mac&sact=del&id={$id}&macid={$macid}\" onclick=\"return confirm('确认要删除吗？');\">删除</a></td>\n";
		echo "</tr>\n";
	}
	?>
</table>
<?php
}
?>
<div style="position:fixed;right:10px;bottom:10px;"><a href="index.php" class="red">返回首页</a></div>
</body>
</html>